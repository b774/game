class Ai:
    def __init__(self, game, colour, capture, unsecure_secure, unsecure, secure):
        self.capture = capture
        self.unsecure_secure = unsecure_secure
        self.unsecure = unsecure
        self.secure = secure
        self.game = game
        self.colour = colour

    def make_move(self):

        state_self = self.game.board.get_state(self.colour)
        state_enemy = self.game.board.get_state_enemy(self.colour)

        state = state_self[0]
        prison = state_self[1]

        enemy_state = state_enemy[0]

        dice = self.game.dice

        if prison > 0:
            options = self.get_options_prison(state, enemy_state, dice)
        else:
            options = self.get_options(state, enemy_state, dice)

        if options == "No options":
            # print("No options")
            return

        best_option = self.get_best_option(options)
        # print("best option (value, position, die), die.number = {0}:".format(dice[best_option[2]].number))
        # print(best_option)

        self.game.dice[best_option[2]].selected()
        self.game.move(best_option[1])
        # print("New red state: {0}: \nNew black state: {1}:".format(self.game.board.get_state("red"), self.game.board.get_state("black")))

    def can_move(self, game, position, moves):
        finishing = self.check_if_finishing(game, self.colour)
        prison = game.board.get_state(self.colour)[1] > 0

        count = game.board.get_state(self.colour)[0]
        count_enemy = game.board.get_state_enemy(self.colour)[0]

        if position == -1 and prison == 0:
            return False
        # Check that you have a marker to move in that position
        if count[position] == 0 and position != -1:
            return False
        # Check if in prison and trying to move other
        if prison and position != -1:
            return False
        # Check if too many enemies
        if count_enemy[23-moves-position] > 1 and 23-moves-position >= 0:
            return False
        # Check if trying to move out without being ready
        if not finishing and position+moves > 23:
            return False

        return True

    @staticmethod
    def check_if_finishing(game, colour):
        finishing = True
        state = game.board.get_state(colour)

        for i in range(18):
            finishing = finishing and state[0][i] == 0

        return finishing and state[1] == 0

    def secures(self, position, moves, state):
        if self.check_if_finishing(self.game, self.colour):
            return True
        if state[position+moves] == 1:
            return True

        if (state[position] == 1 or state[position] >= 3) and state[position+moves] >= 1:
            return True

        return False

    def unsecures(self, position, moves, state):
        if self.check_if_finishing(self.game, self.colour):
            if state[position] > 2:
                return False
            return True
        if state[position+moves] == 0 or state[position] == 2:
            return True

        return False

    @staticmethod
    def captures(position, moves, enemy_state):
        if enemy_state[23-position-moves] == 1:
            return True

        return False

    def get_options_prison(self, state, enemy_state, dice):
        options = {}
        for i in range(len(dice)):
            if dice[i].state == "used":
                continue
            if self.can_move(self.game, -1, dice[i].number):
                value = self.get_value_from_option(-1, dice[i].number, state, enemy_state)
                options[i] = (value, -1, i)

        # print("prison options: {0}".format(options))
        if len(options) == 0:
            return "No options"
        return options

    def get_options(self, state, enemy_state, dice):
        options = {}
        positions_to_move_from = [position for position in range(len(state)) if state[position] > 0]

        key = 0
        for position in positions_to_move_from:
            for i in range(len(dice)):
                if dice[i].state == "used":
                    continue
                if self.can_move(self.game, position, dice[i].number):
                    value = self.get_value_from_option(position, dice[i].number, state, enemy_state)
                    options[key] = (value, position, i)
                    key += 1

        if len(options) == 0:
            return "No options"

        # print("options: {0}".format(options))
        return options

    def get_value_from_option(self, position, moves, state, enemy_state):
        if self.captures(position, moves, enemy_state):
            return self.capture * (23-position-moves)

        unsecures = self.unsecures(position, moves, state)
        secures = self.secures(position, moves, state)

        if unsecures and secures:
            return self.unsecure_secure * moves

        if unsecures:
            return self.unsecure * (-position)

        if secures:
            return self.secure * (position+moves)

        return -100

    @staticmethod
    def get_best_option(options):
        best_option = -100
        best_option_key = list(options.keys())[0]
        for key in options:
            option = options[key]
            if option[0] > best_option:
                best_option = option[0]
                best_option_key = key

        # print("best option key: {0}".format(best_option_key))
        return options[best_option_key]
