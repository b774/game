import random

from ai.simple.Ai import Ai
from game.Game import Game


class TestSuite:
    def __init__(self, generations, epochs, randomness,
                 start_capture, start_unsecure_secure, start_unsecure, start_secure):
        self.generations = generations
        self.epochs = epochs
        self.randomness = randomness

        self.start_capture = start_capture
        self.start_unsecure_secure = start_unsecure_secure
        self.start_unsecure = start_unsecure
        self.start_secure = start_secure

        self.generation_red_capture = start_capture
        self.generation_red_unsecure_secure = start_unsecure_secure
        self.generation_red_unsecure = start_unsecure
        self.generation_red_secure = start_secure

        self.generation_black_capture = start_capture
        self.generation_black_unsecure_secure = start_unsecure_secure
        self.generation_black_unsecure = start_unsecure
        self.generation_black_secure = start_secure

    def new_attributes(self):
        self.generation_red_capture = random.uniform(self.start_capture-self.randomness, self.start_capture+self.randomness)
        self.generation_black_capture = random.uniform(self.start_capture-self.randomness, self.start_capture+self.randomness)

        self.generation_red_unsecure_secure = random.uniform(self.start_unsecure_secure-self.randomness, self.start_unsecure_secure+self.randomness)
        self.generation_black_unsecure_secure = random.uniform(self.start_unsecure_secure-self.randomness, self.start_unsecure_secure+self.randomness)

        self.generation_red_unsecure = random.uniform(self.start_unsecure-self.randomness, self.start_unsecure+self.randomness)
        self.generation_black_unsecure = random.uniform(self.start_unsecure-self.randomness, self.start_unsecure+self.randomness)

        self.generation_red_secure = random.uniform(self.start_secure-self.randomness, self.start_secure+self.randomness)
        self.generation_black_secure = random.uniform(self.start_secure-self.randomness, self.start_secure+self.randomness)

    def create_ais(self, game):
        ai_red = Ai(game, "red",
                    self.generation_red_capture, self.generation_red_unsecure_secure,
                    self.generation_red_unsecure, self.generation_red_secure)
        ai_black = Ai(game, "black",
                      self.generation_black_capture, self.generation_black_unsecure_secure,
                      self.generation_black_unsecure, self.generation_black_secure)

        return ai_red, ai_black

    def play_generation(self):
        score_red = 0
        score_black = 0
        for i in range(self.generations):
            game = Game()
            ai_red, ai_black = self.create_ais(game)

            while game.winner is None:
                if game.colour_to_move == "red":
                    ai_red.make_move()
                else:
                    ai_black.make_move()

            if game.winner == "red":
                score_red += 1
            elif game.winner == "black":
                score_black += 1

        print("final score: {0} - {1}".format(score_red, score_black))

        if score_red >= score_black:
            self.start_secure = self.generation_red_secure
            self.start_unsecure = self.generation_red_unsecure
            self.start_capture = self.generation_red_capture
            self.start_unsecure_secure = self.generation_red_unsecure_secure
        else:
            self.start_secure = self.generation_black_secure
            self.start_unsecure = self.generation_black_unsecure
            self.start_capture = self.generation_black_capture
            self.start_unsecure_secure = self.generation_black_unsecure_secure

    def play_epochs(self):
        for i in range(self.epochs):
            self.new_attributes()
            self.randomness = self.randomness/1.5

            self.play_generation()

            print("For Red\ncapture: {3}\nunsecure_secure: {2}\nunsecure: {1}\nsecure: {0}\n".format(
                self.generation_red_secure, self.generation_red_unsecure,
                self.generation_red_unsecure_secure, self.generation_red_capture
            ))

            print("For Black\ncapture: {3}\nunsecure_secure: {2}\nunsecure: {1}\nsecure: {0}\n".format(
                self.generation_black_secure, self.generation_black_unsecure,
                self.generation_black_unsecure_secure, self.generation_black_capture
            ))
