class Board:
    def __init__(self):
        start_count = [2, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 5,
                       0, 0, 0, 0, 3, 0,
                       5, 0, 0, 0, 0, 0]

        self.state_red = start_count.copy()
        self.prison_red = 0

        self.state_black = start_count.copy()
        self.prison_black = 0

    def get_state(self, colour):
        return [self.state_red, self.prison_red] if colour == "red" \
            else [self.state_black, self.prison_black]

    def get_state_enemy(self, colour):
        return [self.state_red, self.prison_red] if colour == "black" \
            else [self.state_black, self.prison_black]

    def remove_prison_count(self, colour):
        if colour == "red":
            self.prison_red -= 1
            return
        self.prison_black -= 1

    def add_enemy_prison_count(self, colour):
        if colour == "red":
            self.prison_black += 1
            return
        self.prison_red += 1
