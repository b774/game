import random


class Dice:
    def __init__(self, sides, number):
        self.sides = sides
        self.number = number
        self.state = "available"

    def roll(self):
        self.number = random.randint(1, self.sides)

    def available(self):
        self.state = "available"

    def used(self):
        self.state = "used"

    def selected(self):
        if self.state == "used":
            return

        # If already selected then unselect
        if self.state == "selected":
            self.state = "available"
            return

        self.state = "selected"
