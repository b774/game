import os
import sys
import re
from math import floor

import pygame as pg


class Marker:
    def __init__(self, radius):
        self.radius = radius

    def draw(self, dice_size, screen, size, colour, place, count):
        if place == "indicator":
            position = (self.radius, round(screen.height/2))
            pg.draw.circle(color=colour, center=position, radius=self.radius, surface=screen.screen)

        for i in range(count):
            too_big = self.radius*2*(i+1) > round(size[1]/2) - self.radius
            if too_big:
                break
            position = self.calculate_position_from_place(dice_size, size, place, colour, i)
            pg.draw.circle(color=colour, center=position, radius=self.radius, surface=screen)

    def calculate_position_from_place(self, dice_size, screen_size, place, colour, number):
        pos_x = 0
        pos_y = 0
        x_offset = round(dice_size/2)
        if place == -1:
            return (round(screen_size[0]/2)+self.radius, round(screen_size[1]/2)) if colour == "red" \
                else (round(screen_size[0]/2)-self.radius, round(screen_size[1]/2))
        if colour == "red":
            if place < 12:
                pos_x = screen_size[0] - x_offset - dice_size*place
                pos_y = self.radius + self.radius*2*number
            else:
                pos_x = x_offset + dice_size*(place-12)
                pos_y = screen_size[1] - self.radius - self.radius*2*number
        elif colour == "black":
            if place < 12:
                pos_x = screen_size[0] - x_offset - dice_size*place
                pos_y = screen_size[1] - self.radius - self.radius*2*number
            else:
                pos_x = x_offset + dice_size*(place-12)
                pos_y = self.radius + self.radius*2*number

        return pos_x, pos_y


class Dice:
    def __init__(self, size):
        self.normal = None
        self.selected = None
        self.used = None
        self.get_images(size)

    def get_images(self, size):
        path_images = "images"
        normal = []
        selected = []
        used = []
        for filename in os.listdir(path_images):
            path = os.path.join(path_images, filename)
            if re.match(r"dice\d.png", filename):
                normal.append(pg.transform.scale(pg.image.load(path), (size, size)).convert())
            elif re.match(r"dice\d_selected.png", filename):
                selected.append(pg.transform.scale(pg.image.load(path), (size, size)).convert())
            elif re.match(r"dice\d_used.png", filename):
                used.append(pg.transform.scale(pg.image.load(path), (size, size)).convert())

        self.normal = normal
        self.selected = selected
        self.used = used

    def draw(self, screen, kind, position, number):
        if kind == "selected":
            image = self.selected[number-1]
        elif kind == "used":
            image = self.used[number-1]
        else:
            image = self.normal[number-1]

        screen.blit(image, position)


class Screen:
    def __init__(self, width, height, marker_size):
        self.game = None
        self.size = width, height
        self.width = width
        self.height = height
        self.screen = pg.display.set_mode(self.size)
        self.dice_size = round(width/12)
        self.dice = Dice(self.dice_size)
        self.marker_radius = round(width*marker_size)
        self.marker = Marker(self.marker_radius)
        self.board_image = pg.transform.scale(pg.image.load("images/board.jpg"), (width, height))

    def start(self, game):
        pg.init()
        self.game = game
        self.game.roll_dice()

    def draw(self):
        self.screen.blit(self.board_image, (0, 0))

        red_state = self.game.board.get_state("red")
        red_on_board = red_state[0]
        red_prison = red_state[1]
        black_state = self.game.board.get_state("black")
        black_on_board = black_state[0]
        black_prison = black_state[1]
        for n in range(len(red_on_board)):
            self.marker.draw(self.dice_size, self.screen, self.size, "red", n, red_on_board[n])
            self.marker.draw(self.dice_size, self.screen, self.size, "black", n, black_on_board[n])
        if red_prison > 0:
            self.marker.draw(self.dice_size, self.screen, self.size, "red", -1, 1)
        if black_prison > 0:
            self.marker.draw(self.dice_size, self.screen, self.size, "black", -1, 1)

        self.marker.draw(self.dice_size, self, self.size, self.game.colour_to_move, "indicator", 0)

        dice = self.game.dice
        position = (self.width - len(dice)*self.dice_size, round((self.height - self.dice_size)/2))

        for die in dice:
            self.dice.draw(self.screen, die.state, position, die.number)
            position = (position[0]+self.dice_size, position[1])

    @staticmethod
    def update():
        pg.display.update()

    def event(self, events):
        for event in events:
            if event.type == pg.QUIT:
                sys.exit()

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_1:
                    die = self.game.dice[0]
                    die.selected()
                elif event.key == pg.K_2:
                    die = self.game.dice[1]
                    die.selected()

            if event.type == pg.MOUSEBUTTONDOWN:
                click_coordinates = pg.mouse.get_pos()
                position = convert_coordinates_to_position(self, click_coordinates)
                if position[0]:
                    self.game.move(position[1])

    @staticmethod
    def end(winner):
        print(winner)
        pg.end()


def convert_coordinates_to_position(screen, coordinates):
    game = screen.game
    colour = screen.game.colour_to_move

    marker_on_coord = check_if_marker_on_coord(game, screen, coordinates, colour)
    if marker_on_coord[0]:
        if marker_on_coord[1] == -1:
            # If on prison
            if round(screen.width/2) < coordinates[0] < round(screen.width/2) + screen.dice_size:
                return True, -1
        return marker_on_coord

    return False, 404


# This needs abstraction, very bruteforcy right now
def check_if_marker_on_coord(game, screen, coordinates, colour):
    x_cord = coordinates[0]
    position = floor(x_cord/screen.dice_size)
    y_cord = coordinates[1]

    state = game.board.get_state(colour)

    # If red and clicking top half removing prison area, then we start from 11 and go down
    if colour == "red" and y_cord < round(screen.height/2) - screen.marker_radius:
        count = state[0][11 - position]
        # If actually clicking the stack
        if count*screen.marker_radius*2 > y_cord:
            return count > 0, 11-position

        return False, position
    # If red and clicking bottom half removing prison area, then we need to look 12 forward
    if colour == "red" and y_cord > round(screen.height/2) + screen.marker_radius:
        count = state[0][12 + position]
        # If actually clicking the stack
        if screen.height - count*screen.marker_radius*2 < y_cord:
            return count > 0, 12+position

        return False, position

    # If black and clicking bottom half removing prison area, then we need to look 12 forward
    if colour == "black" and y_cord > round(screen.height/2) + screen.marker_radius:
        count = state[0][11 - position]
        # If actually clicking the stack
        if screen.height - count*screen.marker_radius*2 < y_cord:
            return count > 0, 11-position

        return False, position
    # If black and clicking top half removing prison area, then we start from 11 and go down
    if colour == "black" and y_cord < round(screen.height/2) - screen.marker_radius:
        count = state[0][12 + position]
        # If actually clicking the stack
        if count*screen.marker_radius*2 > y_cord:
            return count > 0, 12+position

        return False, position

    return game.board.get_state(colour)[1] > 0, -1
