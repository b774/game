import random

from game.Board import Board
from game.Dice import Dice
from game import Marker


class Game:
    def __init__(self):
        self.board = Board()
        self.dice = [Dice(6, 1), Dice(6, 1)]
        self.status = "ok"
        self.colour_to_move = random.choice(["red", "black"])
        self.winner = None

    def change_status(self, status):
        self.status = status

    def roll_dice(self):
        self.dice = [Dice(6, 1), Dice(6, 1)]
        for die in self.dice:
            die.roll()
            die.available()

        number = self.dice[0].number
        if number == self.dice[1].number:
            self.dice.append(Dice(6, number))
            self.dice.append(Dice(6, number))

        # print("\n\n\n")
        # n = 0
        # for die in self.dice:
            # print("dice{0}: {1}".format(n, die.number))

    def next_player(self):
        self.roll_dice()
        self.colour_to_move = "red" if self.colour_to_move == "black" else "black"

        self.check_if_all_dice_used_or_no_valid_move()

    def move(self, position):
        # print("Red state: {0}: \nBlack state: {1}:".format(self.board.get_state("red"), self.board.get_state("black")))
        for die in self.dice:
            if die.state == "selected":
                if Marker.move(self, position, die.number, self.colour_to_move):
                    die.used()
                    self.check_if_all_dice_used_or_no_valid_move()
                    return True
                die.selected()
                return False

        for die in self.dice:
            if die.state == "used":
                continue

            if Marker.move(self, position, die.number, self.colour_to_move):
                die.used()
                self.check_if_all_dice_used_or_no_valid_move()
                return True

            return False

    def check_if_all_dice_used_or_no_valid_move(self):
        for die in self.dice:
            if die.state == "used":
                continue

            position = 0
            state = self.board.get_state(self.colour_to_move)

            if Marker.check_if_valid(self, -1, die.number, self.colour_to_move):
                return False

            for n in state[0]:
                # If marker on position
                if n > 0:
                    if not Marker.check_if_valid(self, position, die.number, self.colour_to_move):
                        position += 1
                        continue
                    return False
                position += 1

        self.next_player()
        return True

    def won(self, colour):
        print(colour + " has won the game!")
        self.winner = colour
