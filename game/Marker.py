def move(game, position, steps, colour):
    finishing = check_if_finishing(game, colour)
    prison = check_if_prison(game, colour)
    valid = check_if_valid(game, position, steps, colour)

    count = game.board.get_state(colour)
    count_enemy = game.board.get_state_enemy(colour)

    if valid:
        # If going out
        if finishing and position+steps > 23:
            count[0][position] -= 1
            if check_if_won(game, colour):
                game.won(colour)
            return True

        # If escaping prison
        if prison:
            game.board.remove_prison_count(colour)
        else:
            count[0][position] -= 1
        # Add to new spot
        count[0][position+steps] += 1

        # If capturing
        if count_enemy[0][23-steps-position] == 1:
            count_enemy[0][23-steps-position] -= 1
            game.board.add_enemy_prison_count(colour)
        return True

    return False


def check_if_finishing(game, colour):
    finishing = True
    state = game.board.get_state(colour)

    for i in range(18):
        finishing = finishing and state[0][i] == 0

    return finishing and state[1] == 0


def check_if_won(game, colour):
    won = True
    state = game.board.get_state(colour)

    for i in range(17, 24):
        won = won and state[0][i] == 0

    return won and state[1] == 0


def check_if_prison(game, colour):
    return game.board.get_state(colour)[1] > 0


def check_if_valid(game, position, steps, colour):
    finishing = check_if_finishing(game, colour)
    prison = check_if_prison(game, colour)

    state = game.board.get_state(colour)
    count = state[0]
    prison_count = state[1]
    count_enemy = game.board.get_state_enemy(colour)[0]

    if position == -1 and prison_count == 0:
        return False
    # Check that you have a marker to move in that position
    if count[position] == 0 and position != -1:
        return False
    # Check if in prison and trying to move other
    if prison and position != -1:
        return False
    # Check if too many enemies
    if count_enemy[23-steps-position] > 1 and 23-steps-position >= 0:
        return False
    # Check if trying to move out without being ready
    if not finishing and position+steps > 23:
        return False

    return True


