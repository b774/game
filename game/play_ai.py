from game.Game import Game

from ai.simple.Ai import Ai

red_score = 0
black_score = 0
for i in range(1000):
    game = Game()

    ai1_colour = "red"
    ai1 = Ai(game, ai1_colour, 1, 1, 1, 1)
    ai2_colour = "black"
    ai2 = Ai(game, ai2_colour, 9.3, 9.4, 10.6, 9.5)

    while game.winner is None:
        if game.colour_to_move == ai1_colour:
            ai1.make_move()
        else:
            ai2.make_move()

    if game.winner == "red":
        red_score += 1
    else:
        black_score += 1

print(red_score)
print(black_score)
