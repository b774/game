from Game import Game
from Draw import Screen
import pygame as pg

from ai.simple.Ai import Ai

game = Game()
screen = Screen(800, 600, 1/35)
screen.start(game)

ai_colour = "red"
ai = Ai(game, ai_colour, 9.3, 9.4, 10.6, 9.5)

while game.winner is None:
    if game.colour_to_move == ai_colour:
        pg.time.wait(1000)
        ai.make_move()

    screen.event(pg.event.get())
    screen.draw()
    screen.update()

exit(0)
