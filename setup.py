from distutils.core import setup

setup(
    name='Backgammon',
    version='0.1dev',
    packages=['backgammon'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
)
